import os
import json


def inset_image_with_descriptors(data, centers):
    query = 'CREATE (i:Image { path : "' + data.get('path') + '", vec: ' + json.dumps(data.get('vec')) + '})' + os.linesep
    query += "WITH i" + os.linesep
    for item in centers:
        query += "CREATE (i)-[:DESC]->(:Sift {desc: " + json.dumps(item.tolist()) + "})" + os.linesep
    # query += "CREATE (i)-[:DESC]->(:Sift {desc: [1, 1, 1]})" + os.linesep
    # query += "CREATE (i)-[:DESC]->(:Sift {desc: [2, 2, 2]})" + os.linesep
    # query += "CREATE (i)-[:DESC]->(:Sift {desc: [3, 3, 3]})" + os.linesep

    # query += "CREATE (i)-[:DESC]->(:Sift {desc: [3, 3, 3]})" + os.linesep
    # query += "CREATE (i)-[:DESC]->(:Sift {desc: [2, 2, 2]})" + os.linesep
    # query += "CREATE (i)-[:DESC]->(:Sift {desc: [1, 1, 1]})" + os.linesep

    query += "WITH i" + os.linesep
    #query += "MATCH (i)-[:DESC]->(s:Sift)" + os.linesep
    query += "RETURN id(i) as imgId" + os.linesep
    #query += "ORDER BY id(s) ASC" + os.linesep
    return query


def get_complement(id):
    # query = "MATCH (i:Image)-[r:DESC]->(s:Sift)" + os.linesep
    # query += "WHERE id(i) <>" + str(id) + os.linesep
    # query += "RETURN id(i) as imgId, s as sift" + os.linesep
    # query += "ORDER BY id(s) ASC" + os.linesep

    query = "MATCH (i:Image)" + os.linesep
    query += "WHERE id(i) <>" + str(id) + os.linesep
    query += "RETURN i"
    return query


def merge_ed(id1, id2, value):
    query = "MATCH (a:Image), (b:Image)" + os.linesep
    query += "WHERE id(a) = " + str(id1) + " AND id(b) = " + str(id2) + os.linesep
    query += "MERGE (a)-[r:ED]-(b)" + os.linesep
    query += "SET r.value = " + str(value) + os.linesep
    return query


def set_final_vec(iid, vec):
    query = "MATCH (i:Image)" + os.linesep
    query += "WHERE id(i) = " + str(iid) + os.linesep
    query += "SET i.vec = " + json.dumps(vec) + os.linesep
    return query
