import cv2
import numpy as np
from neo4j.v1 import GraphDatabase, basic_auth
import math
import queries
import sys
import os
from itertools import izip
import imghdr
import json

# config
config = {
    'K': 128,
    'database': {
        'user': 'neo4j',
        'password': 'root',
        'host': 'localhost'
    }
}
K = 128

# database
driver = GraphDatabase.driver("bolt://localhost",
                              auth=basic_auth(config['database']['user'], config['database']['password']))
session = driver.session()

i = iter(sys.argv[1:])
defs = dict(izip(i, i))

if len(sys.argv) <= 1 or defs.get('-i', None) is None:
    print ('usage: sift.py -i <image> ')
    exit(2)

absPath = os.path.abspath(defs.get('-i'))
if not os.path.isfile(absPath) or imghdr.what(absPath) not in ('jpeg'):
    print (absPath + ' is not a jpeg file or does not exist')
    exit(2)

# image processing
img = cv2.imread(absPath)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# initialize SIFT
sift = cv2.xfeatures2d.SIFT_create()

# extract descriptors
kp, desc = sift.detectAndCompute(gray, None)
desc = np.float32(desc)

# Define criteria = ( type, max_iter = 10 , epsilon = 1.0 )
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

# Set flags
flags = cv2.KMEANS_RANDOM_CENTERS

# Apply KMeans
compactness, labels, centers = cv2.kmeans(desc, config['K'], None, criteria, 10, flags)

meanByColumn = []
for i in range(0, config['K']):
    ssum = 0
    for item in centers:
        ssum += item[i]
    meanByColumn.append(ssum / config['K'])

# Insert item and its descriptors
result = session.run(queries.inset_image_with_descriptors({'path': absPath, 'vec': json.dumps(meanByColumn)}, centers))
currentImageId = list(result)[0]['imgId']
result = session.run(queries.get_complement(currentImageId))


# Compute and store E-distance
for record in result:
    total = 0
    arr = json.loads(record['i']['vec'])
    for i in range(0, config['K']):
        total += pow(meanByColumn[i] - arr[i], 2)
    ed = math.sqrt(total)
    session.run(queries.merge_ed(currentImageId, record['i'].id, ed))

session.close()

# draw keypoints
# img = cv2.drawKeypoints(gray, kp, None, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# cv2.imwrite('../images/IMG_0352_sift_out.jpg', img)
