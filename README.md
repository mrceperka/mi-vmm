# Exploration of multimedia database
1. Obtain SIFT descriptors from image
2. Cluster image descriptors using **kmeans**
3. Compute **Euclidean distance** between current image and all other images
4. TODO - Cluster images in database
5. TODO - Reduce dimension to 3
6. TODO - Visualize using three.js
7. Explore