module.exports = {
	env: process.env.NODE_ENV || 'development',
  	host: process.env.HOST || 'localhost',
  	port: process.env.PORT || 3000,
  	threshold: 15000,
  	db: {
  		user: 'neo4j',
  		password: 'root',
  		host: 'localhost',
  		port: 7474
  	}
}