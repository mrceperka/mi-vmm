import * as actions from '../constants/actions';
import Immutable from 'immutable';
const apiDefaultState = {
  api: {
    url: '/api'
  },
  nodes: Immutable.List(),
  actual: {},
  history: {
    items: Immutable.List(),
    map: Immutable.OrderedMap(),
  }
};

export const apiReducer = (state = apiDefaultState, action) => {
  switch (action.type) {
    case actions.FETCH_TN_SUCCEEDED:
      return {
        ...state,
        nodes: Immutable.List(action.payload)
      };
    case actions.FETCH_RNG_SUCCEEDED: {
      const actual = action.payload[0];
      return {
        ...state,
        actual: actual,
        history: {
          items: state.history.items.push(actual),
          map: state.history.map.set(actual.id, actual)
        }
      };
    }
    case actions.SELECT_NODE: {
      const actual = state.nodes.find((n) => n.id === action.payload);
      return {
        ...state,
        actual: actual,
        history: {
          items: state.history.items.push(actual),
          map: state.history.map.set(actual.id, actual)
        }
      };
    }
    case actions.SELECT_NODE_HISTORY: {
      const actual = state.history.map.get(action.payload);
      return {
        ...state,
        actual: actual
      };
    }
    default:
      return state;
  }
};


const notificationsDefaultState = {
  fetch: {
    rng: {
      fired: 0,
      state: 'none'
    },
    tn: {
      state: 'none'
    }
  },
  start_node_selected: false,
  message: '',
  show_history: false,
  show_actual: false
};
export const notificationsReducer = (state = notificationsDefaultState, action) => {
  switch (action.type) {
    case actions.FETCH_RNG_STARTED:
      return {
        ...state,
        fetch: {
          ...state.fetch,
          rng: {
            ...state.fetch.rng,
            fired: state.fetch.rng.fired + 1,
            state: 'started'
          }
        },
        message: actions.FETCH_RNG_STARTED
      };
    case actions.FETCH_RNG_SUCCEEDED:
      return {
        ...state,
        fetch: {
          ...state.fetch,
          rng: {
            ...state.fetch.rng,
            state: 'succeeded'
          }
        },
        start_node_selected: true,
        message: actions.FETCH_RNG_SUCCEEDED
      };
    case actions.FETCH_TN_STARTED:
      return {
        ...state,
        tn: {
          ...state.fetch.tn,
          state: 'started'
        },
        message: actions.FETCH_TN_STARTED
      };
    case actions.FETCH_TN_SUCCEEDED:
      return {
        ...state,
        tn: {
          ...state.fetch.tn,
          state: 'succeeded'
        },
        message: actions.FETCH_TN_SUCCEEDED
      };
    case actions.TOGGLE_HISTORY:
      return {
        ...state,
        show_history: !state.show_history
      };
    case actions.TOGGLE_ACTUAL:
      return {
        ...state,
        show_actual: !state.show_actual
      };
    default:
      return state;
  }
};