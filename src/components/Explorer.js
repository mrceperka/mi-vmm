import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {fetchTN, fetchRNG, selectNode, toggle, selectNodeHistory} from "../actions";
import {TOGGLE_HISTORY} from "../constants/actions";

import TopBarNotif from "./TopBarNotif";
import SVGArea from "./SVGArea";
import Images from "./Images";
import History from "./History";
import Actual from "./Actual";
import Settings from "./Settings"

import FlatButton from 'material-ui/FlatButton';
import IconButton from "material-ui/IconButton";
import ActionHistoryIcon from "material-ui/svg-icons/action/history";
import RaisedButton from "material-ui/RaisedButton";
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from "material-ui/Toolbar";

class Explorer extends Component {
  restart() {
    const {actions} = this.props;
    return actions.fetchRNG().then(() => {
      actions.fetchTN();
    });
  }

  componentDidMount() {
    this.restart();
  }
  render() {
    const {actions, exp} = this.props;
    return (
        <div>
          <Toolbar>
            <ToolbarGroup style={{marginLeft: 10}}>
              <FlatButton href="/" label="MI-VMM Explorer"/>
            </ToolbarGroup>
            <ToolbarGroup firstChild={true}>
              <TopBarNotif {...this.props}/>
            </ToolbarGroup>
            <ToolbarGroup>
              <Settings {...this.props}/>
              <Actual {...this.props}/>
              <IconButton title="History" touch={true} onClick={() => actions.toggle(TOGGLE_HISTORY)} disabled={exp.history.length <= 1}>
                <ActionHistoryIcon />
              </IconButton>
              <RaisedButton label="RNG" onClick={() => this.restart()} primary={true} />
            </ToolbarGroup>
          </Toolbar>
          <History {...this.props}/>
          <Images {...this.props}/>

        </div>
    );
  }
}

//<div style={{display: 'flex'}}>
//  <div style={{flexGrow: 3}}>
//    <SVGArea {...this.props}/>
//  </div>
//</div>

export default connect(
    state => ({
      exp: state.exp,
      notif: state.notif
    }),
    dispatch => ({
      actions: bindActionCreators({
        fetchTN,
        fetchRNG,
        selectNode,
        toggle,
        selectNodeHistory
      }, dispatch)
    })
)(Explorer)
