import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';

import IconButton from "material-ui/IconButton";
import ThresholdSlider from "./ThresholdSlider";
import ActionCheckCircleIcon from "material-ui/svg-icons/action/check-circle";

export default class Settings extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    return (
        <div>
          <IconButton title="Show actual image" touch={true} onClick={this.handleTouchTap}>
            <ActionCheckCircleIcon />
          </IconButton>
          <Popover
              open={this.state.open}
              anchorEl={this.state.anchorEl}
              anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              onRequestClose={this.handleRequestClose}
          >
            <div>
              <ThresholdSlider {...this.props}/>
            </div>
          </Popover>
        </div>
    );
  }
}