import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React, {Component} from "react";
import {Provider} from "react-redux";
import Explorer from "./Explorer";

export default class App extends Component {
  render() {
    const {store} = this.props;
    return (
        <Provider store={store}>
          <MuiThemeProvider>
            <Explorer/>
          </MuiThemeProvider>
        </Provider>
    );
  }
}


