import React, {Component} from "react";

export default class TopBarNotif extends Component {
  render() {
    const {notif} = this.props;

    return (
        <span>{notif.message}</span>
    )
  }
}