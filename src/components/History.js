import React, {Component} from "react";
import Drawer from "material-ui/Drawer";
import Avatar from "material-ui/Avatar";
import Subheader from "material-ui/Subheader";
import List from "material-ui/List/List";
import ListItem from "material-ui/List/ListItem";

import {TOGGLE_HISTORY} from "../constants/actions";

export default class History extends Component {
  render() {
    const {actions, notif: {show_history}, exp: {history}} = this.props;
    return (
      <div>
        <Drawer
          width={150}
          open={show_history}
          openSecondary={true}
          docked={false}
          onRequestChange={() => actions.toggle(TOGGLE_HISTORY)}>
          <Subheader>History</Subheader>
          <List>
          {
              history.items.map((n, i) => {
                 return (
                   <ListItem
                       key={"history" + n.id + i}
                       onClick={() => {actions.selectNodeHistory(n.id)}}
                       leftAvatar={<Avatar
                           src={n.path}
                           size={30}
                           style={{margin: 5}}
                       />}
                   >
                     {n.id}
                   </ListItem>
                 )
                }
              )
          }
          </List>
        </Drawer>
      </div>
    )
  }
}