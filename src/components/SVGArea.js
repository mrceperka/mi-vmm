import React, {Component} from "react";

export default class SVGArea extends Component {
  render() {
    const {exp: {nodes, actual}} = this.props;
    if (nodes.length) {
      return (
        <svg
            style={{width: '100%', height: 'calc(100vh - 60px)'}} viewBox="-500, -500, 1000, 1000" shapeRendering="geometricPrecision">
          <defs>
            {this.pattern(actual)}
            {nodes.map((n) => this.pattern(n))}
          </defs>
          {this.node(actual)}
          {nodes.map((n, i) => this.node(n, i))}
        </svg>
      )
    } else {
      return null;
    }
  }

  pattern(n) {
    return <pattern key={"img" + n.id} id={"img" + n.id} width="1" height="1" x="0" y="0">
      <image xlinkHref={n.path} width="100" height="100"/>
    </pattern>
  }
  node(n, i = 0) {
    const {actions, exp: {actual}} = this.props;

    const level = i > 0 ? Math.ceil(i/12) : 1;
    const angle = Math.PI / 6; //30
    const step = i % 12;
    console.log(level, step, angle * step);
    const xy = this.rotatePoint(-step * angle, {x: 0, y: 1});
    console.log(xy);
    let atrs = {
      onClick: () => actions.selectNode(n.id),
      key: "n" + n.id,
      id: "n" + n.id,
      cx: xy.x * n.ed * level,
      cy: xy.y * n.ed * level,
      r: 50,
      fill: "url(#img" + n.id + ")"
    };
    if(actual.id === n.id) {
      atrs.onClick = null;
      atrs.cx = 0;
      atrs.cy = 0;
      atrs.strokeWidth = 3;
      atrs.stroke = 'red';
    }
    return <circle
        {...atrs}
    />
  }
  rotatePoint(angle, point) {
    let x = point.x * Math.cos(angle) + point.y * Math.sin(angle);
    let y = - point.x * Math.sin(angle) + point.y * Math.cos(angle);
    return {
      x: x,
      y: y
    }
  }
}