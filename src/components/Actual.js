import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import IconButton from "material-ui/IconButton";
import ActionVisibility from "material-ui/svg-icons/action/visibility";
import Close from "material-ui/svg-icons/navigation/close";
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import {TOGGLE_ACTUAL} from "../constants/actions";

export default class Actual extends React.Component {
  handleTouchTap = (event) => {
    const {actions} = this.props;

    // This prevents ghost click.
    event.preventDefault();
    actions.toggle(TOGGLE_ACTUAL);
  };

  handleRequestClose = () => {
    const {actions} = this.props;
    actions.toggle(TOGGLE_ACTUAL)
  };

  render() {
    const {notif, exp: {actual}} = this.props;
    return (
        <div>
          <IconButton title="Show actual image" touch={true} onClick={this.handleTouchTap} disabled={notif.fetch.rng.state !== 'succeeded'}>
            {!notif.show_actual ? <ActionVisibility /> : <Close/>}
          </IconButton>
          <Popover
              open={notif.show_actual}
              anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              onRequestClose={this.handleRequestClose}
          >
            <Card>
              <CardMedia
                overlay={<CardTitle title="Currently selected image" subtitle="Displayed images are somehow similar to this image" />}
              >
                {notif.fetch.rng.state === 'succeeded' ? <img style={{width: 400}} src={actual.path}/> : ''}
              </CardMedia>
            </Card>
          </Popover>
        </div>
    );
  }
}