import React, {Component} from 'react';
import Slider from 'material-ui/Slider';

/**
 * The slider bar can have a set minimum and maximum, and the value can be
 * obtained through the value parameter fired on an onChange event.
 */
export default class ThresholdSlider extends Component {
  state = {
    value: 0.5
  };

  handleFirstSlider = (event, value) => {
    this.setState({value: value});
    //this.props.actions.fetchTN(value)
  };

  render() {
    return (
        <div style={{width: 200, padding: 20}}>
          <Slider
              min={0}
              max={150}
              step={10}
              defaultValue={50}
              value={this.state.value}
              onChange={this.handleFirstSlider}
          />
          <div>
          <span>{this.state.value}</span>
            </div>
        </div>
    );
  }
}