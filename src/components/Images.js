import React, {Component} from "react";

import Avatar from 'material-ui/Avatar';
import {GridList, GridTile} from "material-ui/GridList";
import Chip from 'material-ui/Chip';
import IconButton from "material-ui/IconButton";
import ActionCheckCircleIcon from "material-ui/svg-icons/action/check-circle";
import Compare from "material-ui/svg-icons/image/compare";

export default class Images extends Component {
  render() {
    const {exp: {nodes, history}, actions} = this.props;
    const styles = {
      root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      },
      gridList: {
        width: '99vw',
        //width: 500,
        //height: 450,
        overflowY: 'auto',
      },
    };

    if (!nodes.isEmpty()) {
      return (
          <div style={styles.root}>
            <GridList
                cellHeight={180}
                style={styles.gridList}
                cols={Math.min(nodes.size, 5)}
            >
              {nodes.map((n) => (
                  <GridTile
                      key={n.id}
                      title={
                        <Chip>
                          <Avatar title="E-distance" color="#444" icon={<Compare/>} />
                          {parseInt(n.ed)}
                        </Chip>
                      }
                      actionIcon={
                        history.map.get(n.id, false) ?
                          <IconButton title="Visited">
                            <ActionCheckCircleIcon color="white" />
                          </IconButton>
                        : <span></span>
                      }
                      onClick={() => actions.selectNode(n.id)}
                      style={{cursor: 'pointer'}}
                  >
                    <img src={n.path} />
                  </GridTile>
              ))}
            </GridList>
          </div>
      )
    } else {
      return null;
    }
  }
}