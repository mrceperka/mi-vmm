import * as actions from "../constants/actions";

export function fetchTN() {
  return function (dispatch, getState) {
    const {exp: {api: {url}, actual}} = getState();
    const plusUrl = '/tn/' + actual.id;
    dispatch({type: actions.FETCH_TN_STARTED});
    return fetch(url + plusUrl)
        .then(
            response => response.json()
        ).then(
            json => dispatch({type: actions.FETCH_TN_SUCCEEDED, payload: json})
        )
        .catch(() => dispatch({type: actions.FETCH_TN_FAILED}))
  }
}

export function fetchRNG() {
  return function (dispatch, getState) {
    const {exp: {api: {url}}} = getState();
    const plusUrl = '/rng';
    dispatch({type: actions.FETCH_RNG_STARTED});
    return fetch(url + plusUrl)
        .then(
            response => response.json()
        ).then(
            json => dispatch({type: actions.FETCH_RNG_SUCCEEDED, payload: json})
        )
        .catch(() => dispatch({type: actions.FETCH_RNG_FAILED}))
  }
}

export function selectNode(id) {
  return function (dispatch, getState) {
    dispatch({type: actions.SELECT_NODE, payload: id});
    fetchTN()(dispatch, getState);
  };
}

export function toggle(type) {
  return {type: type};
}

export function selectNodeHistory(id) {
  return function (dispatch, getState) {
    dispatch({type: actions.SELECT_NODE_HISTORY, payload: id});
    fetchTN()(dispatch, getState);
  };
}