import { combineReducers } from 'redux';
import { apiReducer, notificationsReducer } from './reducers';

const rootReducer = combineReducers({
  exp: apiReducer,
  notif: notificationsReducer
});

export default rootReducer;