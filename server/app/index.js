import path from "path";
import Koa from 'koa';
import logger from 'koa-logger';
import Router from 'koa-router';

const neo4j = require('neo4j-driver').v1;
const config = require('./../../config/config.js');
const queries = require('./queries.js');

const apiRouter = new Router({
  prefix: '/api'
});

const app = new Koa();
const db = neo4j.driver("bolt://" + config.db.host, neo4j.auth.basic(config.db.user, config.db.password))

apiRouter.get('/rng/:limit?', async(ctx, next) => {
  ctx.set('Content-Type', 'application/json');
  const params = ctx.params;

  const session = db.session();
  const result = await session.run(queries.rng(), {
    limit: params.limit ? neo4j.int(params.limit) : 1
  });
  session.close();

  let finalBody = [];
  result.records.forEach(function (record) {
    finalBody.push({
      id: record.get('i').identity.toInt(),
      path: 'http://vmm' + record.get('i').properties.path.replace('/var/www/mi-vmm', ''),
    })
  });
  ctx.body = JSON.stringify(finalBody);
});

apiRouter.get('/tn/:id/:threshold?', async(ctx, next) => {
  ctx.set('Content-Type', 'application/json');
  const params = ctx.params;

  const session = db.session();
  const result = await session.run(queries.tn(true), {
      id: neo4j.int(params.id),
      exclude: JSON.stringify([]),
      threshold: params.threshold ? neo4j.int(params.threshold) : config.threshold
    }
  );
  session.close();

  let finalBody = [];
  result.records.forEach(function (record) {
    finalBody.push({
      id: record.get('id').toInt(),
      path: 'http://vmm' + record.get('path').replace('/var/www/mi-vmm', ''),
      ed: record.get('ed')
    })
  });
  ctx.body = JSON.stringify(finalBody);
});


app
  .use(logger())
  .use(apiRouter.routes())
  .use(apiRouter.allowedMethods());

const server = app.listen(
    config.port,
    () => console.log(`Server started: http://${config.host}:${config.port}/`)
);

