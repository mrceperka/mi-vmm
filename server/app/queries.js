const eol = require('os').EOL;
module.exports = {
  tn: (exclude = false) => {
    let query = 'MATCH (ia:Image)-[d:ED]-(ib:Image)' + eol;
    query += 'WHERE id(ia) = {id} AND d.value < {threshold}' + eol;
    if (exclude) {
      query += 'AND NOT id(ib) IN {exclude}' + eol
    }
    query += 'RETURN id(ib) as id, ib.path as path, d.value as ed' + eol;
    query += 'ORDER BY ed ASC' + eol;
    return query;
  },
  rng: () => {
    let query = 'MATCH (i:Image)' + eol;
    query += 'WITH i, rand() AS number' + eol;
    query += 'RETURN i' + eol;
    query += 'ORDER BY number' + eol;
    query += 'LIMIT {limit}' + eol;
    return query;
  }
};